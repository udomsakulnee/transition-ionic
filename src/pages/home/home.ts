import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private onChange(value: string): void {
    // alert(value);
  }

  constructor(public navCtrl: NavController) {

  }

}
