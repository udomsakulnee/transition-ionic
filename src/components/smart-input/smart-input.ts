import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

/**
 * Generated class for the SmartInputComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SmartInputComponent),
  multi: true
};
@Component({
  selector: 'smart-input',
  templateUrl: 'smart-input.html',
  providers: [ CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR ]
})
export class SmartInputComponent {

  private _value: string;

  constructor() {
    
  }

  // อ่านค่า
  get value(): string {
    return this._value;
  };

  // เก็บค่า
  set value(value: string) {
    if (this._value != value) {
      this._value = value;
      alert(value);
      this.onChange(value);
    }
  }

  // เขียนค่าลง input
  public writeValue(value: string) {
    if (value) {
      alert(value);
      this._value = value;
    }
  }

  private onChange = (_) => {};
  private onTouched = () => {};
  public registerOnChange(fn: (_: string) => void): void { this.onChange = fn; }
  public registerOnTouched(fn: () => void): void { this.onTouched = fn; }

}
