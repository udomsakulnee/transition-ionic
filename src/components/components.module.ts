import { NgModule } from "@angular/core";
import { SmartInputComponent } from "./smart-input/smart-input";
import { CommonModule } from "@angular/common";
import { IonicModule } from "ionic-angular";
@NgModule({
  declarations: [SmartInputComponent],
  imports: [CommonModule, IonicModule],
  exports: [SmartInputComponent],
})
export class ComponentsModule {}

